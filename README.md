# corruption-and-climate-shaping-the-future

This demonstration explores how the level of government effort to prevent corruption and address climate change can impact the fate of the environment and its people. Each universe represents a different level of historical government effort (from 1 to 5), and you can travel through time and space to see the consequences of government action or inaction in the past, present and future.
